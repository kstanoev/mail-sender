﻿
using System;
using System.IO;

using MailKit.Net.Smtp;

using MimeKit;

namespace MailSender
{
    class Program
    {
        static void Main()
        {
            // Setup email message
            MimeMessage email = new MimeMessage();
            email.From.Add(new MailboxAddress("")); // Sender email address
            email.To.Add(new MailboxAddress("")); // Recipient email address
            email.Subject = "Sent from C# console application!";

            BodyBuilder builder = new BodyBuilder();
            builder.HtmlBody = @"Hello,<br/>
                                My name is <b>Alphonso De Netto</b>.<br/>
                                Please have a look at the attached file!";
            string pathToFile = @"../../../Images/image-attachment.png"; // Path to the file attachment
            builder.Attachments.Add("sample.png", File.ReadAllBytes(pathToFile));
            email.Body = builder.ToMessageBody();

            // Setup email client
            SmtpClient client = new SmtpClient();
            string smtpServer = "smtp.abv.bg";
            int serverPort = 465;
            bool useSSL = true;
            client.Connect(smtpServer, serverPort, useSSL);
            string username = ""; // abv.bg username
            string password = ""; // abv.bg password
            client.Authenticate(username, password);
            client.Send(email);
            client.Disconnect(true);

            Console.WriteLine("Message Sent...");
        }
    }
}
